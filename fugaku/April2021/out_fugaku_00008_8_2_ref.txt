DM Object: Mesh 32 MPI processes
  type: plex
Mesh in 3 dimensions:
  0-cells: 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125 125
  1-cells: 300 300 300 300 300 300 300 300 300 300 300 300 300 300 300 300 300 300 300 300 300 300 300 300 300 300 300 300 300 300 300 300
  2-cells: 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240 240
  3-cells: 64 64 64 64 64 64 64 64 64 64 64 64 64 64 64 64 64 64 64 64 64 64 64 64 64 64 64 64 64 64 64 64
Labels:
  celltype: 4 strata with value/size (0 (125), 1 (300), 4 (240), 7 (64))
  depth: 4 strata with value/size (0 (125), 1 (300), 2 (240), 3 (64))
  marker: 1 strata with value/size (1 (210))
  Face Sets: 3 strata with value/size (1 (49), 3 (49), 6 (49))
  0 SNES Function norm 6.966083027163e+00 
  Linear solve converged due to CONVERGED_RTOL iterations 17
  1 SNES Function norm 2.548808531575e-12 
Nonlinear solve converged due to CONVERGED_ITS iterations 1
  Linear solve converged due to CONVERGED_RTOL iterations 17
  Linear solve converged due to CONVERGED_RTOL iterations 17
  Linear solve converged due to CONVERGED_RTOL iterations 17
  Linear solve converged due to CONVERGED_RTOL iterations 17
  Linear solve converged due to CONVERGED_RTOL iterations 17
  Linear solve converged due to CONVERGED_RTOL iterations 17
  Linear solve converged due to CONVERGED_RTOL iterations 17
  Linear solve converged due to CONVERGED_RTOL iterations 17
  Linear solve converged due to CONVERGED_RTOL iterations 17
  Linear solve converged due to CONVERGED_RTOL iterations 17
************************************************************************************************************************
***             WIDEN YOUR WINDOW TO 120 CHARACTERS.  Use 'enscript -r -fCourier9' to print this document            ***
************************************************************************************************************************

---------------------------------------------- PETSc Performance Summary: ----------------------------------------------

../ex13 on a arch-arm-gnu named c31-7211c with 32 processors, by a04199 Thu Apr 22 01:56:18 2021
Using 8 OpenMP threads
Using Petsc Development GIT revision: v3.15.0-210-g087e2b54a3  GIT Date: 2021-04-18 19:29:13 -0400

                         Max       Max/Min     Avg       Total
Time (sec):           6.727e-01     1.000   6.727e-01
Objects:              2.253e+03     1.066   2.124e+03
Flop:                 7.386e+07     1.522   6.071e+07  1.943e+09
Flop/sec:             1.098e+08     1.522   9.025e+07  2.888e+09
MPI Messages:         1.753e+04     2.387   1.215e+04  3.888e+05
MPI Message Lengths:  7.200e+06     2.210   4.199e+02  1.633e+08
MPI Reductions:       9.150e+02     1.000

Flop counting convention: 1 flop = 1 real number operation of type (multiply/divide/add/subtract)
                            e.g., VecAXPY() for real vectors of length N --> 2N flop
                            and VecAXPY() for complex vectors of length N --> 8N flop

Summary of Stages:   ----- Time ------  ----- Flop ------  --- Messages ---  -- Message Lengths --  -- Reductions --
                        Avg     %Total     Avg     %Total    Count   %Total     Avg         %Total    Count   %Total
 0:      Main Stage: 4.9387e-01  73.4%  4.4120e+08  22.7%  6.234e+04  16.0%  9.684e+02       37.0%  3.470e+02  37.9%
 1:         PCSetUp: 2.4270e-02   3.6%  2.1567e+07   1.1%  3.099e+03   0.8%  8.036e+02        1.5%  2.900e+01   3.2%
 2:  KSP Solve only: 1.5450e-01  23.0%  1.4800e+09  76.2%  3.233e+05  83.2%  3.105e+02       61.5%  5.200e+02  56.8%

------------------------------------------------------------------------------------------------------------------------
See the 'Profiling' chapter of the users' manual for details on interpreting output.
Phase summary info:
   Count: number of times phase was executed
   Time and Flop: Max - maximum over all processors
                  Ratio - ratio of maximum to minimum over all processors
   Mess: number of messages sent
   AvgLen: average message length (bytes)
   Reduct: number of global reductions
   Global: entire computation
   Stage: stages of a computation. Set stages with PetscLogStagePush() and PetscLogStagePop().
      %T - percent time in this phase         %F - percent flop in this phase
      %M - percent messages in this phase     %L - percent message lengths in this phase
      %R - percent reductions in this phase
   Total Mflop/s: 10e-6 * (sum of flop over all processors)/(max time over all processors)
   GPU Mflop/s: 10e-6 * (sum of flop on GPU over all processors)/(max GPU time over all processors)
   CpuToGpu Count: total number of CPU to GPU copies per processor
   CpuToGpu Size (Mbytes): 10e-6 * (total size of CPU to GPU copies per processor)
   GpuToCpu Count: total number of GPU to CPU copies per processor
   GpuToCpu Size (Mbytes): 10e-6 * (total size of GPU to CPU copies per processor)
   GPU %F: percent flops on GPU in this event
------------------------------------------------------------------------------------------------------------------------
Event                Count      Time (sec)     Flop                              --- Global ---  --- Stage ----  Total   GPU    - CpuToGpu -   - GpuToCpu - GPU
                   Max Ratio  Max     Ratio   Max  Ratio  Mess   AvgLen  Reduct  %T %F %M %L %R  %T %F %M %L %R Mflop/s Mflop/s Count   Size   Count   Size  %F
---------------------------------------------------------------------------------------------------------------------------------------------------------------

--- Event Stage 0: Main Stage

PetscBarrier           2 1.0 5.0873e-03 1.1 0.00e+00 0.0 9.2e+02 3.4e+01 1.0e+01  1  0  0  0  1   1  0  1  0  3     0       0      0 0.00e+00    0 0.00e+00  0
BuildTwoSided         47 1.0 4.2352e-02 6.2 0.00e+00 0.0 4.9e+03 4.0e+00 4.7e+01  3  0  1  0  5   4  0  8  0 14     0       0      0 0.00e+00    0 0.00e+00  0
BuildTwoSidedF        17 1.0 3.5111e-02 8.8 0.00e+00 0.0 2.2e+03 1.4e+04 1.7e+01  2  0  1 18  2   3  0  3 50  5     0       0      0 0.00e+00    0 0.00e+00  0
MatMult              280 1.6 9.3390e-03 1.3 5.84e+06 2.0 3.6e+04 3.1e+02 1.0e+00  1  8  9  7  0   2 34 57 18  0 15858       0      1 1.80e-03    0 0.00e+00 100
MatMultAdd            17 1.0 6.5286e-03 3.7 1.11e+05 1.9 5.3e+02 2.4e+02 0.0e+00  1  0  0  0  0   1  1  1  0  0   415       0      0 0.00e+00    0 0.00e+00 100
MatMultTranspose      17 1.0 1.9148e-03 3.0 1.11e+05 1.8 5.9e+02 2.3e+02 1.0e+00  0  0  0  0  0   0  1  1  0  0  1423       0      0 1.19e-04    0 0.00e+00 100
MatSolve              17 0.0 6.4435e-04 0.0 7.15e+05 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0  1110       0      0 0.00e+00    0 0.00e+00  0
MatLUFactorSym         1 1.0 1.1501e-0398.7 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
MatLUFactorNum         1 1.0 1.6124e-03726.3 1.30e+06 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0   809       0      0 0.00e+00    0 0.00e+00  0
MatConvert             5 1.0 4.7880e-04 1.2 0.00e+00 0.0 7.4e+02 8.0e+01 1.0e+00  0  0  0  0  0   0  0  1  0  0     0       0      0 0.00e+00    0 0.00e+00  0
MatScale               3 1.0 4.7543e-04 2.4 6.80e+04 2.0 3.7e+02 3.1e+02 0.0e+00  0  0  0  0  0   0  0  1  0  0  3612       0      1 1.80e-03    0 0.00e+00  5
MatResidual           17 1.0 1.7888e-03 1.3 1.06e+06 2.0 6.3e+03 3.1e+02 0.0e+00  0  1  2  1  0   0  6 10  3  0 15078   173952      0 0.00e+00    0 0.00e+00 100
MatAssemblyBegin      67 1.0 3.9773e-02 2.5 0.00e+00 0.0 2.2e+03 1.4e+04 1.1e+01  3  0  1 18  1   5  0  3 50  3     0       0      0 0.00e+00    0 0.00e+00  0
MatAssemblyEnd        67 1.0 3.0752e-02 1.1 1.86e+0486.3 0.0e+00 0.0e+00 3.2e+01  4  0  0  0  3   6  0  0  0  9    11       0      0 0.00e+00    0 0.00e+00  0
MatGetRowIJ            1 0.0 1.0947e-04 0.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
MatCreateSubMat        2 1.0 3.7432e-03 1.0 0.00e+00 0.0 1.6e+02 8.4e+02 2.8e+01  1  0  0  0  3   1  0  0  0  8     0       0      0 0.00e+00    0 0.00e+00  0
MatGetOrdering         1 0.0 3.8142e-04 0.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
MatCoarsen             1 1.0 4.6970e-03 1.1 0.00e+00 0.0 3.7e+03 4.0e+02 7.0e+00  1  0  1  1  1   1  0  6  2  2     0       0      0 0.00e+00    0 0.00e+00  0
MatZeroEntries         3 1.0 3.8190e-05 1.3 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
MatAXPY                1 1.0 3.5779e-04 1.4 5.12e+02 1.5 0.0e+00 0.0e+00 1.0e+00  0  0  0  0  0   0  0  0  0  0    40       0      0 0.00e+00    0 0.00e+00  0
MatTranspose           4 1.0 2.7985e-04 1.2 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
MatMatMultSym          3 1.0 2.0507e-03 1.1 0.00e+00 0.0 1.1e+03 2.1e+02 9.0e+00  0  0  0  0  1   0  0  2  0  3     0       0      0 0.00e+00    0 0.00e+00  0
MatMatMultNum          1 1.0 7.3816e-04 1.0 6.14e+04 2.0 3.7e+02 3.1e+02 1.0e+00  0  0  0  0  0   0  0  1  0  0  2110       0      0 0.00e+00    0 0.00e+00  0
MatPtAPSymbolic        1 1.0 7.6648e-03 1.1 0.00e+00 0.0 2.1e+03 6.5e+02 7.0e+00  1  0  1  1  1   2  0  3  2  2     0       0      0 0.00e+00    0 0.00e+00  0
MatPtAPNumeric         1 1.0 7.3872e-03 1.0 5.83e+05 2.5 6.4e+02 6.7e+02 5.0e+00  1  1  0  0  1   1  3  1  1  1  1733       0      0 0.00e+00    0 0.00e+00  0
MatTrnMatMultSym       1 1.0 7.1332e-02 1.0 0.00e+00 0.0 1.8e+03 1.6e+04 1.2e+01 11  0  0 19  1  14  0  3 50  3     0       0      0 0.00e+00    0 0.00e+00  0
MatGetLocalMat         4 1.0 3.3350e-04 1.3 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
MatGetBrAoCol          3 1.0 1.5950e-03 1.8 0.00e+00 0.0 2.6e+03 6.1e+02 0.0e+00  0  0  1  1  0   0  0  4  3  0     0       0      0 0.00e+00    0 0.00e+00  0
KSPSetUp               5 1.0 1.5590e-03 1.0 3.42e+05 1.9 1.8e+03 3.1e+02 1.9e+01  0  0  0  0  2   0  2  3  1  5  5624   24545      0 0.00e+00    0 0.00e+00 100
KSPSolve               1 1.0 1.9041e-02 1.0 5.77e+06 1.6 3.2e+04 3.1e+02 5.2e+01  3  8  8  6  6   4 34 52 17 15  7833   41585      0 1.19e-04    0 0.00e+00 99
SNESSolve              1 1.0 2.5105e-01 1.0 1.17e+07 1.3 5.3e+04 9.5e+02 2.3e+02 37 17 14 31 26  51 75 86 84 67  1314   36142      5 6.54e-03    0 0.00e+00 50
SNESSetUp              1 1.0 4.7024e-02 1.1 0.00e+00 0.0 2.2e+03 2.9e+03 1.8e+01  7  0  1  4  2   9  0  4 10  5     0       0      0 0.00e+00    0 0.00e+00  0
SNESFunctionEval       3 1.0 3.6032e-02 1.1 3.91e+06 1.0 9.2e+02 2.3e+02 4.0e+00  5  6  0  0  0   7 28  1  0  1  3434      60      3 2.82e-03    0 0.00e+00  0
SNESJacobianEval       2 1.0 1.6506e-01 1.0 3.77e+06 1.0 6.9e+02 7.9e+03 2.0e+00 24  6  0  3  0  33 27  1  9  1   725       0      0 0.00e+00    0 0.00e+00  0
DMCreateMat            1 1.0 4.6986e-02 1.1 0.00e+00 0.0 2.2e+03 2.9e+03 1.8e+01  7  0  1  4  2   9  0  4 10  5     0       0      0 0.00e+00    0 0.00e+00  0
Mesh Partition         1 1.0 7.3714e-03 1.0 0.00e+00 0.0 1.6e+02 1.1e+02 8.0e+00  1  0  0  0  1   1  0  0  0  2     0       0      0 0.00e+00    0 0.00e+00  0
Mesh Migration         1 1.0 1.6889e-02 1.0 0.00e+00 0.0 9.0e+02 8.1e+01 2.9e+01  2  0  0  0  3   3  0  1  0  8     0       0      0 0.00e+00    0 0.00e+00  0
DMPlexPartSelf         1 1.0 8.6910e-0499.3 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
DMPlexPartLblInv       1 1.0 4.6252e-03 1.4 0.00e+00 0.0 0.0e+00 0.0e+00 3.0e+00  1  0  0  0  0   1  0  0  0  1     0       0      0 0.00e+00    0 0.00e+00  0
DMPlexPartLblSF        1 1.0 8.2592e-04 5.5 0.00e+00 0.0 6.2e+01 5.6e+01 1.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
DMPlexPartStrtSF       1 1.0 3.6855e-04 1.3 0.00e+00 0.0 3.1e+01 2.2e+02 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
DMPlexPointSF          1 1.0 3.6594e-04 1.3 0.00e+00 0.0 6.2e+01 2.7e+02 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
DMPlexInterp          19 1.0 1.6536e-03 1.1 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
DMPlexDistribute       1 1.0 2.4494e-02 1.0 0.00e+00 0.0 1.1e+03 9.6e+01 3.7e+01  4  0  0  0  4   5  0  2  0 11     0       0      0 0.00e+00    0 0.00e+00  0
DMPlexDistCones        1 1.0 1.4425e-03 1.1 0.00e+00 0.0 1.9e+02 1.4e+02 2.0e+00  0  0  0  0  0   0  0  0  0  1     0       0      0 0.00e+00    0 0.00e+00  0
DMPlexDistLabels       1 1.0 8.7349e-03 1.0 0.00e+00 0.0 4.6e+02 6.4e+01 2.4e+01  1  0  0  0  3   2  0  1  0  7     0       0      0 0.00e+00    0 0.00e+00  0
DMPlexDistField        1 1.0 6.0823e-03 1.0 0.00e+00 0.0 2.2e+02 5.9e+01 2.0e+00  1  0  0  0  0   1  0  0  0  1     0       0      0 0.00e+00    0 0.00e+00  0
DMPlexStratify        30 1.0 2.9946e-03 1.9 0.00e+00 0.0 0.0e+00 0.0e+00 4.0e+00  0  0  0  0  0   0  0  0  0  1     0       0      0 0.00e+00    0 0.00e+00  0
DMPlexSymmetrize      30 1.0 1.9888e-04 1.4 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
DMPlexPrealloc         1 1.0 4.2827e-02 1.0 0.00e+00 0.0 2.2e+03 2.9e+03 1.6e+01  6  0  1  4  2   9  0  4 10  5     0       0      0 0.00e+00    0 0.00e+00  0
DMPlexResidualFE       3 1.0 1.7250e-02 1.1 2.64e+06 1.0 0.0e+00 0.0e+00 0.0e+00  3  4  0  0  0   3 19  0  0  0  4889       0      0 0.00e+00    0 0.00e+00  0
DMPlexJacobianFE       2 1.0 1.6021e-01 1.0 2.94e+06 1.0 4.6e+02 1.2e+04 2.0e+00 23  5  0  3  0  32 21  1  9  1   583       0      0 0.00e+00    0 0.00e+00  0
SFSetGraph            39 1.0 3.4031e-05 1.5 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
SFSetUp               30 1.0 1.1796e-02 1.4 0.00e+00 0.0 7.7e+03 3.6e+02 3.0e+01  2  0  2  2  3   2  0 12  5  9     0       0      0 0.00e+00    0 0.00e+00  0
SFBcastBegin          58 1.0 7.7813e-03 8.9 0.00e+00 0.0 6.8e+03 3.7e+02 0.0e+00  0  0  2  2  0   0  0 11  4  0     0       0      2 2.31e-03    0 0.00e+00  0
SFBcastEnd            58 1.0 3.0804e-02 2.9 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  3  0  0  0  0   4  0  0  0  0     0       0      1 5.10e-04    0 0.00e+00  0
SFReduceBegin         12 1.0 8.4689e-04 3.9 0.00e+00 0.0 1.3e+03 1.8e+03 0.0e+00  0  0  0  1  0   0  0  2  4  0     0       0      0 0.00e+00    0 0.00e+00  0
SFReduceEnd           12 1.0 9.7211e-0311.9 6.51e+02 0.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   1  0  0  0  0     1       0      0 0.00e+00    0 0.00e+00 100
SFFetchOpBegin         2 1.0 1.5883e-0419.1 0.00e+00 0.0 2.3e+02 3.1e+03 0.0e+00  0  0  0  0  0   0  0  0  1  0     0       0      0 0.00e+00    0 0.00e+00  0
SFFetchOpEnd           2 1.0 1.3875e-03 3.7 0.00e+00 0.0 2.3e+02 3.1e+03 0.0e+00  0  0  0  0  0   0  0  0  1  0     0       0      0 0.00e+00    0 0.00e+00  0
SFCreateEmbed          3 1.0 3.9044e-0321.2 0.00e+00 0.0 2.6e+02 4.0e+01 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
SFDistSection          9 1.0 4.4961e-03 1.4 0.00e+00 0.0 1.8e+03 1.3e+02 1.1e+01  1  0  0  0  1   1  0  3  0  3     0       0      0 0.00e+00    0 0.00e+00  0
SFSectionSF           11 1.0 4.7647e-03 3.1 0.00e+00 0.0 1.5e+03 4.9e+02 1.1e+01  0  0  0  0  1   1  0  2  1  3     0       0      0 0.00e+00    0 0.00e+00  0
SFRemoteOff            2 1.0 3.8196e-0315.8 0.00e+00 0.0 4.6e+02 1.0e+02 0.0e+00  0  0  0  0  0   0  0  1  0  0     0       0      0 0.00e+00    0 0.00e+00  0
SFPack               210 1.0 1.1843e-03 1.2 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      3 4.11e-03    0 0.00e+00  0
SFUnpack             212 1.0 6.0008e-04 3.5 1.61e+04109.7 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0    47       0      1 6.29e-04    0 0.00e+00 100
VecTDot               56 1.0 2.9973e-03 2.7 5.73e+04 1.5 0.0e+00 0.0e+00 5.6e+01  0  0  0  0  6   0  0  0  0 16   538    2715      0 0.00e+00    0 0.00e+00 100
VecNorm               25 1.0 2.2906e-03 4.1 2.56e+04 1.5 0.0e+00 0.0e+00 2.5e+01  0  0  0  0  3   0  0  0  0  7   314    2260      0 0.00e+00    0 0.00e+00 100
VecCopy               57 1.0 1.7164e-04 1.3 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
VecSet               131 1.0 1.0714e-03 1.9 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
VecAXPY               55 1.0 6.1031e-04 1.2 5.63e+04 1.5 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0  2598    2809      0 0.00e+00    0 0.00e+00 100
VecAYPX              126 1.0 1.2731e-03 1.1 1.29e+05 1.5 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  1  0  0  0  2853    3161      0 0.00e+00    0 0.00e+00 100
VecAXPBYCZ            34 1.0 4.0600e-04 1.3 8.70e+04 1.5 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  1  0  0  0  6036    6280      0 0.00e+00    0 0.00e+00 100
VecAssemblyBegin       7 1.0 5.3563e-04 1.6 0.00e+00 0.0 0.0e+00 0.0e+00 6.0e+00  0  0  0  0  1   0  0  0  0  2     0       0      0 0.00e+00    0 0.00e+00  0
VecAssemblyEnd         7 1.0 1.9460e-05 1.2 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
VecPointwiseMult      80 1.0 7.2602e-04 1.2 4.10e+04 1.5 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0  1588    1672      0 0.00e+00    0 0.00e+00 100
VecScatterBegin      138 1.0 4.4041e-03 1.4 0.00e+00 0.0 4.1e+04 3.3e+02 5.0e+00  1  0 11  8  1   1  0 66 23  1     0       0      2 3.60e-03    0 0.00e+00  0
VecScatterEnd        138 1.0 9.4017e-03 2.2 1.61e+04 0.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   2  0  0  0  0     2       0      0 1.19e-04    0 0.00e+00 100
VecSetRandom           1 1.0 4.1300e-05 1.5 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
PCGAMGGraph_AGG        1 1.0 7.1256e-03 1.0 6.14e+04 2.0 1.1e+03 1.6e+02 9.0e+00  1  0  0  0  1   1  0  2  0  3   219       0      1 1.80e-03    0 0.00e+00  0
PCGAMGCoarse_AGG       1 1.0 7.8328e-02 1.0 0.00e+00 0.0 8.8e+03 3.8e+03 2.9e+01 12  0  2 20  3  16  0 14 55  8     0       0      0 0.00e+00    0 0.00e+00  0
PCGAMGProl_AGG         1 1.0 1.3805e-03 1.1 0.00e+00 0.0 1.1e+03 6.8e+02 1.5e+01  0  0  0  0  2   0  0  2  1  4     0       0      0 0.00e+00    0 0.00e+00  0
PCGAMGPOpt_AGG         1 1.0 6.4921e-03 1.0 4.04e+05 1.9 4.0e+03 2.4e+02 3.1e+01  1  1  1  1  3   1  2  6  2  9  1591   19112      1 1.80e-03    0 0.00e+00 84
GAMG: createProl       1 1.0 9.3883e-02 1.0 4.66e+05 1.9 1.5e+04 2.3e+03 8.4e+01 14  1  4 22  9  19  3 24 58 24   127   18635      2 3.60e-03    0 0.00e+00 73
  Graph                2 1.0 7.0454e-03 1.0 6.14e+04 2.0 1.1e+03 1.6e+02 9.0e+00  1  0  0  0  1   1  0  2  0  3   221       0      1 1.80e-03    0 0.00e+00  0
  MIS/Agg              1 1.0 4.7274e-03 1.1 0.00e+00 0.0 3.7e+03 4.0e+02 7.0e+00  1  0  1  1  1   1  0  6  2  2     0       0      0 0.00e+00    0 0.00e+00  0
  SA: col data         1 1.0 3.6609e-04 1.1 0.00e+00 0.0 7.4e+02 8.9e+02 6.0e+00  0  0  0  0  1   0  0  1  1  2     0       0      0 0.00e+00    0 0.00e+00  0
  SA: frmProl0         1 1.0 7.0822e-04 1.1 0.00e+00 0.0 3.3e+02 2.1e+02 5.0e+00  0  0  0  0  1   0  0  1  0  1     0       0      0 0.00e+00    0 0.00e+00  0
  SA: smooth           1 1.0 3.0812e-03 1.0 6.85e+04 2.0 1.5e+03 2.3e+02 1.3e+01  0  0  0  0  1   1  0  2  1  4   562    7842      0 0.00e+00    0 0.00e+00  5
GAMG: partLevel        1 1.0 2.0146e-02 1.0 5.83e+05 2.5 3.0e+03 6.2e+02 6.5e+01  3  1  1  1  7   4  3  5  3 19   635       0      0 0.00e+00    0 0.00e+00  0
  repartition          1 1.0 5.1024e-03 1.0 0.00e+00 0.0 3.4e+02 3.9e+02 5.3e+01  1  0  0  0  6   1  0  1  0 15     0       0      0 0.00e+00    0 0.00e+00  0
  Invert-Sort          1 1.0 3.3970e-04 1.3 0.00e+00 0.0 0.0e+00 0.0e+00 6.0e+00  0  0  0  0  1   0  0  0  0  2     0       0      0 0.00e+00    0 0.00e+00  0
  Move A               1 1.0 2.6370e-03 1.0 0.00e+00 0.0 1.6e+02 8.4e+02 1.5e+01  0  0  0  0  2   1  0  0  0  4     0       0      0 0.00e+00    0 0.00e+00  0
  Move P               1 1.0 1.3772e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 1.6e+01  0  0  0  0  2   0  0  0  0  5     0       0      0 0.00e+00    0 0.00e+00  0
PCGAMG Squ l00         1 1.0 7.1335e-02 1.0 0.00e+00 0.0 1.8e+03 1.6e+04 1.2e+01 11  0  0 19  1  14  0  3 50  3     0       0      0 0.00e+00    0 0.00e+00  0
PCGAMG Gal l00         1 1.0 1.5057e-02 1.0 5.83e+05 2.5 2.7e+03 6.5e+02 1.2e+01  2  1  1  1  1   3  3  4  3  3   850       0      0 0.00e+00    0 0.00e+00  0
PCGAMG Opt l00         1 1.0 2.4164e-03 1.0 6.14e+04 2.0 1.5e+03 2.3e+02 1.0e+01  0  0  0  0  1   0  0  2  1  3   645       0      0 0.00e+00    0 0.00e+00  0
PCSetUp                2 1.0 1.2616e-01 1.0 1.96e+06 2.6 2.0e+04 1.9e+03 1.8e+02 18  2  5 23 19  25  8 32 63 50   275   21433      2 3.60e-03    0 0.00e+00 50
PCSetUpOnBlocks       17 1.0 3.1730e-0362.8 1.30e+06 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0   411       0      0 0.00e+00    0 0.00e+00  0
PCApply               17 1.0 1.5976e-02 1.1 4.63e+06 1.6 2.6e+04 3.1e+02 1.0e+00  2  6  7  5  0   3 27 42 13  0  7495   44933      0 1.19e-04    0 0.00e+00 98
DualSpaceSetUp         2 1.0 1.2586e-02 1.1 1.80e+03 1.0 0.0e+00 0.0e+00 0.0e+00  2  0  0  0  0   2  0  0  0  0     5       0      0 0.00e+00    0 0.00e+00  0
FESetUp                2 1.0 6.2624e-03 4.3 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   1  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0

--- Event Stage 1: PCSetUp

BuildTwoSided          1 1.0 4.7188e-03164.5 0.00e+00 0.0 3.1e+01 4.0e+00 1.0e+00  0  0  0  0  0  10  0  1  0  3     0       0      0 0.00e+00    0 0.00e+00  0
BuildTwoSidedF         1 1.0 4.8504e-03133.1 0.00e+00 0.0 6.2e+01 8.9e+03 1.0e+00  0  0  0  0  0  10  0  2 22  3     0       0      0 0.00e+00    0 0.00e+00  0
MatMult                5 1.0 4.8215e-04 1.5 3.07e+05 2.0 1.8e+03 3.1e+02 0.0e+00  0  0  0  0  0   2 36 59 23  0 16154       0      0 0.00e+00    0 0.00e+00 100
MatConvert             2 1.0 3.5900e-06 1.2 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
MatAssemblyBegin       8 1.0 4.8666e-0316.3 0.00e+00 0.0 6.2e+01 8.9e+03 1.0e+00  0  0  0  0  0  11  0  2 22  3     0       0      0 0.00e+00    0 0.00e+00  0
MatAssemblyEnd         8 1.0 6.8284e-03 1.1 2.41e+04 0.0 0.0e+00 0.0e+00 4.0e+00  1  0  0  0  0  28  0  0  0 14     4       0      0 0.00e+00    0 0.00e+00  0
MatZeroEntries         1 1.0 9.7901e-06 9.8 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
MatTranspose           2 1.0 1.2081e-04 1.2 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
MatMatMultSym          2 1.0 3.9521e-04 2.5 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   1  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
MatPtAPSymbolic        1 1.0 1.0348e-02 1.2 0.00e+00 0.0 1.2e+03 1.1e+03 7.0e+00  1  0  0  1  1  39  0 39 55 24     0       0      0 0.00e+00    0 0.00e+00  0
MatPtAPNumeric         1 1.0 1.3429e-02 1.0 5.82e+05 2.3 6.2e+01 8.9e+03 5.0e+00  2  1  0  0  1  55 59  2 22 17   953       0      0 0.00e+00    0 0.00e+00  0
MatGetLocalMat         1 1.0 9.9430e-05 1.4 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
MatGetBrAoCol          1 1.0 2.3408e-03 4.4 0.00e+00 0.0 1.1e+03 1.1e+03 0.0e+00  0  0  0  1  0   7  0 36 49  0     0       0      0 0.00e+00    0 0.00e+00  0
KSPSetUp               2 1.0 1.2694e-03 1.1 3.42e+05 1.9 1.8e+03 3.1e+02 1.7e+01  0  0  0  0  2   5 41 59 23 59  6907   23796      0 0.00e+00    0 0.00e+00 100
SFSetGraph             1 1.0 6.3004e-07 1.4 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
SFPack                 5 1.0 3.9630e-05 1.2 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
SFUnpack               5 1.0 2.2503e-06 1.2 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
VecTDot               11 1.0 3.4605e-04 1.9 1.13e+04 1.5 0.0e+00 0.0e+00 1.1e+01  0  0  0  0  1   1  1  0  0 38   915    2774      0 0.00e+00    0 0.00e+00 100
VecNorm                6 1.0 2.0161e-04 2.1 6.14e+03 1.5 0.0e+00 0.0e+00 6.0e+00  0  0  0  0  1   1  1  0  0 21   857    3158      0 0.00e+00    0 0.00e+00 100
VecCopy                2 1.0 5.9598e-06 1.3 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
VecSet                 2 1.0 1.3000e-05 1.2 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
VecAXPY               10 1.0 1.0061e-04 1.2 1.02e+04 1.5 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  1  0  0  0  2866    3135      0 0.00e+00    0 0.00e+00 100
VecAYPX                4 1.0 4.0950e-05 1.2 4.10e+03 1.5 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  1  0  0  0  2816    3085      0 0.00e+00    0 0.00e+00 100
VecPointwiseMult       6 1.0 5.4910e-05 1.2 3.07e+03 1.5 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0  1575    1653      0 0.00e+00    0 0.00e+00 100
VecScatterBegin        5 1.0 1.5143e-04 1.7 0.00e+00 0.0 1.8e+03 3.1e+02 0.0e+00  0  0  0  0  0   1  0 59 23  0     0       0      0 0.00e+00    0 0.00e+00  0
VecScatterEnd          5 1.0 1.7335e-04 1.6 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   1  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
PCGAMG Gal l00         1 1.0 2.3762e-02 1.1 5.82e+05 2.3 1.3e+03 1.5e+03 1.2e+01  3  1  0  1  1  94 59 41 77 41   539       0      0 0.00e+00    0 0.00e+00  0
PCSetUp                1 1.0 2.5135e-02 1.1 9.24e+05 2.1 3.1e+03 8.0e+02 2.9e+01  4  1  1  2  3 100100100100100   858   23796      0 0.00e+00    0 0.00e+00 41

--- Event Stage 2: KSP Solve only

MatMult              850 1.0 7.8806e-02 1.4 5.22e+07 2.0 3.1e+05 3.1e+02 0.0e+00 10 68 80 60  0  44 89 97 97  0 16801       0      0 0.00e+00    0 0.00e+00 100
MatMultAdd           170 1.0 3.5381e-02 2.0 1.11e+06 1.9 5.3e+03 2.4e+02 0.0e+00  5  1  1  1  0  20  2  2  1  0   766       0      0 0.00e+00    0 0.00e+00 100
MatMultTranspose     170 1.0 1.4502e-02 3.9 1.11e+06 1.8 5.3e+03 2.4e+02 0.0e+00  1  1  1  1  0   3  2  2  1  0  1879       0      0 0.00e+00    0 0.00e+00 100
MatSolve             170 0.0 6.4362e-03 0.0 7.15e+06 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0  1112       0      0 0.00e+00    0 0.00e+00  0
MatLUFactorSym         1 1.0 1.1301e-03111.9 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
MatLUFactorNum         1 1.0 1.6243e-03972.7 1.30e+06 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0   803       0      0 0.00e+00    0 0.00e+00  0
MatResidual          170 1.0 1.7312e-02 1.3 1.06e+07 2.0 6.3e+04 3.1e+02 0.0e+00  2 14 16 12  0  10 18 19 19  0 15579   174616      0 0.00e+00    0 0.00e+00 100
MatGetRowIJ            1 0.0 1.1312e-04 0.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
MatGetOrdering         1 0.0 3.7217e-04 0.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
KSPSetUp               1 1.0 5.1991e-07 1.5 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
KSPSolve              10 1.0 1.5439e-01 1.0 5.77e+07 1.6 3.2e+05 3.1e+02 5.2e+02 23 76 83 61 57 100100100100100  9586   42404      0 0.00e+00    0 0.00e+00 99
SFPack              1190 1.0 7.6558e-03 1.4 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   4  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
SFUnpack            1190 1.0 3.7621e-0310.0 1.61e+05 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0    43       0      0 0.00e+00    0 0.00e+00 100
VecTDot              340 1.0 2.0829e-02 3.8 3.48e+05 1.5 0.0e+00 0.0e+00 3.4e+02  2  1  0  0 37   8  1  0  0 65   470    3064      0 0.00e+00    0 0.00e+00 100
VecNorm              180 1.0 3.5025e-03 1.2 1.84e+05 1.5 0.0e+00 0.0e+00 1.8e+02  0  0  0  0 20   2  0  0  0 35  1480    3198      0 0.00e+00    0 0.00e+00 100
VecCopy              530 1.0 1.7444e-03 1.4 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   1  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
VecSet               700 1.0 7.0889e-03 2.7 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   2  0  0  0  0     0       0      0 0.00e+00    0 0.00e+00  0
VecAXPY              340 1.0 3.3865e-03 1.2 3.48e+05 1.5 0.0e+00 0.0e+00 0.0e+00  0  1  0  0  0   2  1  0  0  0  2894    3149      0 0.00e+00    0 0.00e+00 100
VecAYPX             1180 1.0 1.1893e-02 1.2 1.21e+06 1.5 0.0e+00 0.0e+00 0.0e+00  2  2  0  0  0   7  2  0  0  0  2860    3178      0 0.00e+00    0 0.00e+00 100
VecAXPBYCZ           340 1.0 3.3323e-03 1.2 8.70e+05 1.5 0.0e+00 0.0e+00 0.0e+00  0  1  0  0  0   2  2  0  0  0  7354    7704      0 0.00e+00    0 0.00e+00 100
VecPointwiseMult     680 1.0 6.2681e-03 1.2 3.48e+05 1.5 0.0e+00 0.0e+00 0.0e+00  1  1  0  0  0   4  1  0  0  0  1564    1639      0 0.00e+00    0 0.00e+00 100
VecScatterBegin     1190 1.0 2.6067e-02 1.7 0.00e+00 0.0 3.2e+05 3.1e+02 0.0e+00  3  0 83 61  0  14  0100100  0     0       0      0 0.00e+00    0 0.00e+00  0
VecScatterEnd       1190 1.0 5.5815e-02 1.5 1.61e+05 0.0 0.0e+00 0.0e+00 0.0e+00  8  0  0  0  0  33  0  0  0  0     3       0      0 0.00e+00    0 0.00e+00 100
PCSetUp                1 1.0 3.2183e-0334.6 1.30e+06 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0   405       0      0 0.00e+00    0 0.00e+00  0
PCSetUpOnBlocks      170 1.0 3.3038e-0320.0 1.30e+06 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0   395       0      0 0.00e+00    0 0.00e+00  0
PCApply              170 1.0 1.2439e-01 1.1 4.63e+07 1.6 2.6e+05 3.1e+02 0.0e+00 18 61 67 50  0  78 80 81 81  0  9533   46304      0 0.00e+00    0 0.00e+00 99
---------------------------------------------------------------------------------------------------------------------------------------------------------------

Memory usage is given in bytes:

Object Type          Creations   Destructions     Memory  Descendants' Mem.
Reports information only for process 0.

--- Event Stage 0: Main Stage

           Container    29             29        16936     0.
                SNES     1              1         1540     0.
              DMSNES     1              1          680     0.
       Krylov Solver     6              6        10312     0.
     DMKSP interface     1              1          664     0.
              Matrix   113            119      2670868     0.
      Matrix Coarsen     1              1          632     0.
    Distributed Mesh   114            114       546288     0.
            DM Label   206            206       131840     0.
          Quadrature   149            149        89400     0.
           Index Set   664            663       626892     0.
   IS L to G Mapping     3              4         7720     0.
             Section   213            213       153360     0.
   Star Forest Graph   230            230       253728     0.
     Discrete System   170            170       153684     0.
           Weak Form   171            171       140904     0.
    GraphPartitioner    30             30        20880     0.
              Vector    87             87       276392     0.
              Viewer     2              1          848     0.
      Preconditioner     6              6         6576     0.
        Linear Space     2              2         1376     0.
          Dual Space    26             26        24544     0.
            FE Space     2              2         1592     0.
       Field over DM     1              1          712     0.
         PetscRandom     2              2         1340     0.

--- Event Stage 1: PCSetUp

              Matrix    10              4       284668     0.
           Index Set     2              3         3492     0.
   IS L to G Mapping     1              0            0     0.
   Star Forest Graph     1              1         1224     0.
              Vector     5              5        17024     0.

--- Event Stage 2: KSP Solve only

              Matrix     1              1       256972     0.
           Index Set     3              3         5076     0.
========================================================================================================================
Average time to get PetscTime(): 6.39819e-08
Average time for MPI_Barrier(): 2.56002e-06
Average time for zero size MPI_Send(): 6.57562e-06
#PETSc Option Table entries:
-benchmark_it 10
-dm_distribute
-dm_mat_type aijkokkos
-dm_plex_box_dim 3
-dm_plex_box_faces 2,4,4
-dm_plex_box_lower 0,0,0
-dm_plex_box_simplex 0
-dm_plex_box_upper 1,2,2
-dm_refine 2
-dm_vec_type kokkos
-dm_view
-ksp_converged_reason
-ksp_max_it 150
-ksp_norm_type unpreconditioned
-ksp_rtol 1.e-12
-ksp_type cg
-log_view
-matptap_via scalable
-mg_levels_esteig_ksp_max_it 5
-mg_levels_esteig_ksp_type cg
-mg_levels_ksp_max_it 2
-mg_levels_ksp_type chebyshev
-mg_levels_pc_type jacobi
-pc_gamg_agg_nsmooths 1
-pc_gamg_coarse_eq_limit 2000
-pc_gamg_coarse_grid_layout_type spread
-pc_gamg_esteig_ksp_max_it 5
-pc_gamg_esteig_ksp_type cg
-pc_gamg_process_eq_limit 500
-pc_gamg_repartition false
-pc_gamg_reuse_interpolation true
-pc_gamg_square_graph 1
-pc_gamg_threshold 0.01
-pc_gamg_threshold_scale .5
-pc_gamg_type agg
-pc_type gamg
-petscpartitioner_simple_node_grid 2,2,2
-petscpartitioner_simple_process_grid 1,2,2
-petscpartitioner_type simple
-potential_petscspace_degree 2
-snes_converged_reason
-snes_max_it 1
-snes_monitor
-snes_rtol 1.e-8
-snes_type ksponly
#End of PETSc Option Table entries
Compiled without FORTRAN kernels
Compiled with full precision matrices (default)
sizeof(short) 2 sizeof(int) 4 sizeof(long) 8 sizeof(void*) 8 sizeof(PetscScalar) 8 sizeof(PetscInt) 4
Configure options: --download-fblaslapack=1 --with-debugging=0 CC=mpicc CXX=mpiCC FC=mpif90 COPTFLAGS="-Ofast -march=armv8.2-a+sve -msve-vector-bits=512 -DLANDAU_LAYOUT_LEFT -DLANDAU_MAX_Q=4" CXXOPTFLAGS="-Ofast -march=armv8.2-a+sve -msve-vector-bits=512 -DLANDAU_LAYOUT_LEFT -DLANDAU_MAX_Q=4" --download-kokkos-cmake-arguments="-DKokkos_ENABLE_OPENMP=ON -DKokkos_ENABLE_AGGRESSIVE_VECTORIZATION=ON -DKokkos_ARCH_A64FX=ON" --download-cmake=https://github.com/Kitware/CMake/releases/download/v3.20.1/cmake-3.20.1.tar.gz PETSC_ARCH=arch-arm-gnu --download-p4est --download-zlib --with-ctable=0 --download-kokkos=1 --download-kokkos-kernels=1 --with-openmp=1 --download-kokkos-commit=origin/develop --download-kokkos-kernels-commit=origin/develop --ignoreLinkOutput=1
-----------------------------------------
Libraries compiled on 2021-04-20 15:58:56 on e34-7109c 
Machine characteristics: Linux-4.18.0-240.8.1.el8_3.aarch64-aarch64-with-redhat-8.3-Ootpa
Using PETSc directory: /home/ra010009/a04199/petsc
Using PETSc arch: arch-arm-gnu
-----------------------------------------

Using C compiler: mpicc  -fPIC -Wall -Wwrite-strings -Wno-strict-aliasing -Wno-unknown-pragmas -fstack-protector -fvisibility=hidden -Ofast -march=armv8.2-a+sve -msve-vector-bits=512 -DLANDAU_LAYOUT_LEFT -DLANDAU_MAX_Q=4 -fopenmp   
Using Fortran compiler: mpif90  -fPIC -Wall -ffree-line-length-0 -Wno-unused-dummy-argument -g -O  -fopenmp    
-----------------------------------------

Using include paths: -I/home/ra010009/a04199/petsc/include -I/home/ra010009/a04199/petsc/arch-arm-gnu/include
-----------------------------------------

Using C linker: mpicc
Using Fortran linker: mpif90
Using libraries: -Wl,-rpath,/home/ra010009/a04199/petsc/arch-arm-gnu/lib -L/home/ra010009/a04199/petsc/arch-arm-gnu/lib -lpetsc -Wl,-rpath,/home/ra010009/a04199/petsc/arch-arm-gnu/lib -L/home/ra010009/a04199/petsc/arch-arm-gnu/lib -Wl,-rpath,/home/share/mpi/headers/FJSVpxtof/FX1000/V4.0L20/lib -L/home/share/mpi/headers/FJSVpxtof/FX1000/V4.0L20/lib -Wl,-rpath,/vol0004/apps/oss/mpigcc/fjmpi-gcc8/lib64 -L/vol0004/apps/oss/mpigcc/fjmpi-gcc8/lib64 -Wl,-rpath,/vol0004/apps/oss/spack-v0.16/opt/spack/linux-rhel8-a64fx/gcc-8.3.1/gcc-10.2.0-goql5ifkmfu5pp7kqp2ah63rryrbn772/lib:/vol0004/apps/oss/spack-v0.16/opt/spack/linux-rhel8-a64fx/gcc-8.3.1/gcc-10.2.0-goql5ifkmfu5pp7kqp2ah63rryrbn772/lib64 -Wl,-rpath,/vol0004/apps/oss/spack-v0.16/opt/spack/linux-rhel8-a64fx/gcc-8.3.1/gcc-10.2.0-goql5ifkmfu5pp7kqp2ah63rryrbn772/lib64 -L/vol0004/apps/oss/spack-v0.16/opt/spack/linux-rhel8-a64fx/gcc-8.3.1/gcc-10.2.0-goql5ifkmfu5pp7kqp2ah63rryrbn772/lib64 -Wl,-rpath,/vol0004/apps/oss/spack-v0.16/opt/spack/linux-rhel8-a64fx/gcc-8.3.1/gcc-10.2.0-goql5ifkmfu5pp7kqp2ah63rryrbn772/lib/gcc/aarch64-unknown-linux-gnu/10.2.0 -L/vol0004/apps/oss/spack-v0.16/opt/spack/linux-rhel8-a64fx/gcc-8.3.1/gcc-10.2.0-goql5ifkmfu5pp7kqp2ah63rryrbn772/lib/gcc/aarch64-unknown-linux-gnu/10.2.0 -Wl,-rpath,/vol0004/apps/oss/spack-v0.16/opt/spack/linux-rhel8-a64fx/gcc-8.3.1/gcc-10.2.0-goql5ifkmfu5pp7kqp2ah63rryrbn772/lib -L/vol0004/apps/oss/spack-v0.16/opt/spack/linux-rhel8-a64fx/gcc-8.3.1/gcc-10.2.0-goql5ifkmfu5pp7kqp2ah63rryrbn772/lib -Wl,-rpath,/vol0004/apps/oss/spack-v0.16/opt/spack/linux-rhel8-a64fx/gcc-8.3.1/zstd-1.4.5-5527xh5nso62m6jmxt2h7eje3mpn2rkg/lib -L/vol0004/apps/oss/spack-v0.16/opt/spack/linux-rhel8-a64fx/gcc-8.3.1/zstd-1.4.5-5527xh5nso62m6jmxt2h7eje3mpn2rkg/lib -Wl,-rpath,/vol0004/apps/oss/spack-v0.16/opt/spack/linux-rhel8-a64fx/gcc-8.3.1/zlib-1.2.11-wytstv2k3w7gvjao7xbf6zlqpogktrmb/lib -L/vol0004/apps/oss/spack-v0.16/opt/spack/linux-rhel8-a64fx/gcc-8.3.1/zlib-1.2.11-wytstv2k3w7gvjao7xbf6zlqpogktrmb/lib -Wl,-rpath,/vol0004/apps/oss/spack-v0.16/opt/spack/linux-rhel8-a64fx/gcc-8.3.1/mpc-1.1.0-qce7snyi742unqatwnngsoaxc5pykl5m/lib -L/vol0004/apps/oss/spack-v0.16/opt/spack/linux-rhel8-a64fx/gcc-8.3.1/mpc-1.1.0-qce7snyi742unqatwnngsoaxc5pykl5m/lib -Wl,-rpath,/vol0004/apps/oss/spack-v0.16/opt/spack/linux-rhel8-a64fx/gcc-8.3.1/mpfr-4.0.2-ybonjjb6pixc2aa7orkxz2kcn5ma5n3y/lib -L/vol0004/apps/oss/spack-v0.16/opt/spack/linux-rhel8-a64fx/gcc-8.3.1/mpfr-4.0.2-ybonjjb6pixc2aa7orkxz2kcn5ma5n3y/lib -Wl,-rpath,/vol0004/apps/oss/spack-v0.16/opt/spack/linux-rhel8-a64fx/gcc-8.3.1/isl-0.21-z6p5qrg5oogt565lzsa2ui2mkl7lf6ch/lib -L/vol0004/apps/oss/spack-v0.16/opt/spack/linux-rhel8-a64fx/gcc-8.3.1/isl-0.21-z6p5qrg5oogt565lzsa2ui2mkl7lf6ch/lib -Wl,-rpath,/vol0004/apps/oss/spack-v0.16/opt/spack/linux-rhel8-a64fx/gcc-8.3.1/gmp-6.1.2-v7t3rhfracjljwg6jja44njlx323dg7z/lib -L/vol0004/apps/oss/spack-v0.16/opt/spack/linux-rhel8-a64fx/gcc-8.3.1/gmp-6.1.2-v7t3rhfracjljwg6jja44njlx323dg7z/lib -lkokkoskernels -lkokkoscontainers -lkokkoscore -lp4est -lsc -lflapack -lfblas -lm -lz -lX11 -lstdc++ -ldl -lmpi_usempif08 -lmpi_usempi_ignore_tkr -lmpi_mpifh -lmpi -lgfortran -lm -lgfortran -lm -lgcc_s -lpthread -lstdc++ -ldl
-----------------------------------------

