#!/usr/bin/env python
import sys, os, math, glob
import matplotlib.pyplot as plt
import numpy as npy
import matplotlib.ticker as ticker
#import random as rand
import locale
import pandas as pd
locale.setlocale(locale.LC_ALL, '')
plt.rcParams["font.size"] = 11
#plt.rcParams["font.weight"] = "bold"
#plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams.update({'font.size': 11})
#plt.rcParams.update({'font.weight': "bold"})
nnodes = npy.empty(5, dtype=int)
solve_timeT = npy.empty([5,4])
max_proc_id = -1
max_ref_id = -1
machine_name = ''
for filename in sys.argv[1:]:
    words = filename.split('_')
    machine_name = words[1]
    nds = int(words[2])
    nps = nds*4
    proc_id = int(math.log(nps,8)) 
    if proc_id > max_proc_id: max_proc_id = proc_id
    nnodes[proc_id] = nds
    refine_id = int(words[4]) - 2
    if refine_id < 13:
        if refine_id > max_ref_id: max_ref_id = refine_id
        idx2 = 0
        for text in open(filename,"r"): 
            words = text.split()
            n = len(words)
            if n > 1 and words[0] == 'KSPSolve':
                idx2 = idx2 + 1
                if idx2 == 2:
                    stime = float(words[3])
                    solve_timeT[proc_id,refine_id] = stime
if max_ref_id == -1: print ('no DATA -- need files as arguments !!!!!!')
solve_time = solve_timeT[0:max_proc_id+1,0:max_ref_id+1]
marks2 = [['ro-','bo-.','mo--','go:'],['r^-', 'b^-.', 'm^--', 'g^:'],['r*-', 'b*-.', 'm*--', 'g*:'],['r+-', 'b+-.', 'm+--', 'g+:'],['rx-', 'bx-.', 'mx--', 'gx:'],['rv-', 'bv-.', 'mv--', 'gv:'],['rD-', 'bD-.', 'mD--', 'gD:'],['ro-', 'bo-.', 'mo--', 'go:'],['r|-', 'b|-.', 'm|--', 'g|:'],['rp-', 'bp-.', 'mp--', 'gp:'],['r*-', 'b*-.', 'm*--', 'g*:']]
#
# Weak scaling
#
series_name_full = ['4$\cdot8^3$ cells/node', '4$\cdot 16^3$ cells/node', '4$\cdot 32^3$ cells/node', '4$\cdot 64^3$ cells/node']
series_name_short = ['4$\cdot 8^3$', '4$\cdot 16^3$', '4$\cdot 32^3$', '4$\cdot 64^3$']
nnodes = nnodes[0:max_proc_id+1];

print(nnodes)
df = pd.DataFrame(data=solve_time, index=nnodes, columns=series_name_full[0:max_ref_id+1])
df2 = pd.DataFrame(data=solve_time, index=nnodes, columns=series_name_short[0:max_ref_id+1])
df2.index.name = 'Nodes'
df2.columns.name = 'Cells/node:'
#print df
ax = df.plot(lw=2, colormap='jet', marker='s', markersize=10, logx=True, logy=False,  grid=True, legend=False, fontsize=16)
title='3D Q2 Laplacian AMG solve time (10 solves)'
ax.set_title(title,pad=20, fontdict={'fontsize':16})
patches, labels = ax.get_legend_handles_labels()
ax.legend(patches, labels, loc='lower right', fontsize=16)
xmin, xmax, ymin, ymax = plt.axis()
#xmin, xmax, ymin, ymax = plt.axis([xmin*.9, xmax*1.1, 0, 10])
ax.set_xlabel('# ' + machine_name + ' nodes', fontdict={'fontsize':16})
ax.set_ylabel('AMG Solve Time (rtol=$\mathbf{10^{-12}}$)', fontdict={'fontsize':16})
plt.savefig('weak_scaling_' + machine_name + '.png',bbox_inches='tight')
#latex table

print(df2.to_latex(longtable=False,escape=False,float_format="{:0.2f}".format, caption='Scaled speedup, solve phase time (10 solves) as function of number of Fugaku nodes', label='tab:'))

# caption = '3D Q2 Laplacian solve times: ' + run_type_str + ' - ' + sub_type_str,  , label='tab:GAMGtimes-'+run_type_str+'-'+sub_type_str
