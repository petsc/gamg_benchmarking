Performance data for ECP GAMG benchmarking. Each machine has a directory with data files (eg, Perlmutter and Frontier are the most recent).

## Test code in the PETSC repository: petsc/src/snes/tests/ex13.c

$ `cd petsc_dir/src/snes/tests`\
$ `make ex13`

## Running tests

Each data file (files that start with "out", have a listing of the
options used at the end of the file, such as:

`PETSc Option Table entries (each option on separate line as in the data file):`

>PETSc Option Table entries:\
>-benchmark_it 1 # (source: file)\
>-build_twosided allreduce # (source: file)\
>-dm_mat_type aijkokkos # (source: file)\
>-dm_plex_box_faces 32,32,32 # (source: command line)\
>-dm_plex_box_lower 0,0,0 # (source: file)\
>-dm_plex_box_upper 4,2,1 # (source: file)\
> ....
  
 These lines can be put in a .petscrc file in the program working
 directory.
 
 The "out" files also have PETSc version information, for example:
 
 `Using Petsc Development GIT revision: v3.20.4-590-g26f22146603  GIT Date: 2024-02-07 12:58:08 -0500`

And configure options, eg:

`Configure options: --PETSC_ARCH=arch-kokkos-opt --with-debugging=0 --with-make-np=8 ...`

## Plotting

Plots can be generated within each machine directory with, for example:

$ `cd Perlmutter`\
$ `../plot.py out*`

