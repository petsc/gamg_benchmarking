#!/usr/bin/env python3
#                  0   1 2      3       4 5         6:                            1*       2         3         4*                   5               6
# plot out_* where out_*_KOKKOS_Crusher_*_AMD-MI250_GAMG_Q3_chebyshev_hem.txt is: #nodes, back-end, machine,  #refinement levels, processor type, solver
#  all but [1] and [4] should be the same
#
#
import sys, os, math, glob
import matplotlib.pyplot as plt
import numpy as npy
import matplotlib.ticker as ticker
import random as rand
from numpy import array
import locale
import pandas as pd
locale.setlocale(locale.LC_ALL, '')
plt.rcParams["font.size"] = 11
#plt.rcParams["font.weight"] = "bold"
#plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams.update({'font.size': 11})
#plt.rcParams.update({'font.weight': "bold"})
idx = 0
nnode_s = [0] * 6
nn_ref_SolveSetupNeq = npy.zeros([5,4,3]) # [node_id,refine_id,solve|setup|#eq]
max_thing = npy.zeros(3) 
nn_ref_SolveSetupNeq[:,:,:] = npy.nan
refId_maxNodeID = [-1] * 5 # same as first index of nn_ref_SolveSetupNeq
max_ref_id = -1
min_node_id = -1
min_ref_id = -1
for filename in sys.argv[1:]:
    base = filename.split('.')
    words = base[0].split('_')
    print(words)
    nnodes = int(words[1])
    lang = words[2]
    machine = words[3]
    solver = words[6]
    order = words[7]
    smoother = words[8]
    coarse = words[9]
    if min_ref_id == -1: min_ref_id =  int(words[4]) # this has to be lex order files w/ out*
    else:
      if machine == 'Frontier':
          device = 'GCD'
          device_full = '8 AMD MI250X GCD'  
          device_node = '8'
          cores_node = '8'
          if lang == 'hip':
            lang = 'hipsparse'
      else:
          device = 'GPU'  # Perlmutter
          device_full = '4 NVIDIA A100 GPU' 
          device_node = '4'
          cores_node = '8'
    ref_id = int(words[4]) - min_ref_id
    if ref_id > max_ref_id: max_ref_id = ref_id
    node_id = int(math.log(nnodes,8)) # index of num node (x) index 
    #print('nnodes,node_id = ',nnodes,node_id) 
    if node_id > refId_maxNodeID[ref_id]: refId_maxNodeID[ref_id] = node_id
    nnode_s[node_id] = nnodes # this has to be lex order files w/ out*
    #print ('numprocs=', nnodes, node_id, machine, solver, ref_id)
    n_ksp_solve = 0
    for text in open(filename,"r"): 
        words = text.split()
        n = len(words)
        if n > 2 and words[0] == 'KSPSolve': # and words[1] == '10':
            stime = float(words[3])
            nn_ref_SolveSetupNeq[node_id,ref_id,0] = stime # clobbers to get last
            if stime > max_thing[0] and n_ksp_solve == 1: max_thing[0] = stime
            n_ksp_solve = n_ksp_solve + 1
        elif n > 1 and words[0] == 'PCSetUp':
            stime = float(words[3])
            nn_ref_SolveSetupNeq[node_id,ref_id,1] = stime # clobbers to get last
            if stime > max_thing[1] : max_thing[1] = stime
        if n > 4 and words[2] == 'N': # clobbers to get last
            v = float(words[4].replace(",",""))
            print(words,': proc id = ',node_id,', refine id = ', ref_id,', N = ',v)
            nn_ref_SolveSetupNeq[node_id,ref_id,2] = v
            if v > max_thing[2] : max_thing[2] = v
    idx = idx + 1
if max_ref_id == -1: print ('no DATA -- need files as arguments !!!!!!')
max_refId_maxNodeID = max(refId_maxNodeID);
print('nn_ref_SolveSetupNeq = ',nn_ref_SolveSetupNeq)
solve_time = nn_ref_SolveSetupNeq[:max_refId_maxNodeID+1,0:max_ref_id+1,0:2]
print (solve_time)
####solve_time[:,:,2] = solve_time[:,:,2] - solve_time[:,:,1]
series_name_short = [''] * 4
series_name_full = [''] * 4
print('max_ref_id=',max_ref_id,', refId_maxNodeID=',refId_maxNodeID)
for idx in range(max_ref_id+1):
    print ('ref ',idx,') refId_maxNodeID[idx] = ',refId_maxNodeID[idx])
    num_dof = nn_ref_SolveSetupNeq[refId_maxNodeID[idx],idx,2]
    dof_node_k = int(num_dof/(1000*pow(8,refId_maxNodeID[idx])))
    print ('ref ',idx,') num dof max = ',num_dof,', num NODES = ', pow(8,refId_maxNodeID[idx]), ', dof_node_k = ', dof_node_k )
    series_name_short[idx] = format(dof_node_k, ",") + 'K'
    series_name_full[idx] = format(dof_node_k, ",") + 'K dof/node'
    print ('idx = ',idx, 'dof_node_k = ', dof_node_k, '; series names (short, long): ', series_name_short[idx], series_name_full[idx])
marks2 = [['ro-','bo-.','mo--','go:'],['r^-', 'b^-.', 'm^--', 'g^:'],['r*-', 'b*-.', 'm*--', 'g*:'],['r+-', 'b+-.', 'm+--', 'g+:'],['rx-', 'bx-.', 'mx--', 'gx:'],['rv-', 'bv-.', 'mv--', 'gv:'],['rD-', 'bD-.', 'mD--', 'gD:'],['ro-', 'bo-.', 'mo--', 'go:'],['r|-', 'b|-.', 'm|--', 'g|:'],['rp-', 'bp-.', 'mp--', 'gp:'],['r*-', 'b*-.', 'm*--', 'g*:']]
#
# Weak scaling - GPU
#

#print ('max_refId_maxNodeID = ',max_refId_maxNodeID)
node_arr = nnode_s[0:max_refId_maxNodeID+1]
#print(nproc_arr)
type_name = ['V(2,2)' + ', ' + smoother + ' smooth ' + coarse.upper() + ' coarsen', solver + ' setup (' + machine + '-'+lang+ ')' ]
type_tag = ['solve','setup']
ylabel = ['Solve Time (sec) (rtol=$\mathbf{10^{-12}}$)', 'setup time (sec)']
xlabel = [' - # ' + device_full + ' nodes', ' - # nodes ('+cores_node+' ' + machine + ' cores/node)']
prob = '3D ' + order + ' Laplacian, '
for idx in range(2):
    #print(solve_time[:max_refId_maxNodeID+1,:max_ref_id+1,idx])
    df =  pd.DataFrame(data=solve_time[:max_refId_maxNodeID+1,:max_ref_id+1,idx], index=node_arr, columns=series_name_full[0:max_ref_id+1])
    df2 = pd.DataFrame(data=solve_time[:max_refId_maxNodeID+1,:max_ref_id+1,idx], index=node_arr, columns=series_name_short[0:max_ref_id+1])
    df2.index.name = 'Nodes (' + device_node + ' ' + device + 's/node)'
    df2.columns.name = 'dof/'+device+':'
    ax = df.plot(lw=2, colormap='jet', marker='s', markersize=10, logx=True,logy=False,  grid=True, legend=False, fontsize=16)
    title=prob + type_name[idx]
    ax.set_title(title,pad=20, fontdict={'fontsize':16})
    patches, labels = ax.get_legend_handles_labels()
    ax.legend(patches, labels, loc='best', fontsize=16)
    xmin, xmax, ymin, ymax = plt.axis()
    ymax = max_thing[idx] 
    xmin, xmax, ymin, ymax = plt.axis([xmin*.9, xmax*1.1, 0, 1.1*ymax])
    ax.set_xlabel(machine + xlabel[idx], fontdict={'fontsize':16})
    ax.set_ylabel(ylabel[idx], fontdict={'fontsize':16})
    plt.savefig('weak_scaling_' + type_tag[idx] + '_' + machine + '_' + solver + '_' + lang + '_' + order + '_' + smoother + '_' + coarse + '.png',bbox_inches='tight')
    #latex table
    print(df2.to_latex(longtable=False,escape=False,float_format="{:0.2f}".format, caption=prob + '-- ' + type_name[idx], label='tab:' + '_' + lang + machine + '_' + type_tag[idx] + '_' + order))
