#!/usr/bin/env python3
#      0     1   2  3               4 
# plot local-001-Q2-gpu_aware_mpi_1-gamg.txt
#  all but [1] should be the same
#
#
import sys, os, math, glob
import matplotlib.pyplot as plt
import numpy as npy
import matplotlib.ticker as ticker
import random as rand
from numpy import array
import locale
import pandas as pd
locale.setlocale(locale.LC_ALL, '')
plt.rcParams["font.size"] = 11
#plt.rcParams["font.weight"] = "bold"
#plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams.update({'font.size': 11})
#plt.rcParams.update({'font.weight': "bold"})
idx = 0
max_n_deg = 3
solve_data = npy.zeros([5,2,max_n_deg]) # [node_idx, solv_type_idx, degree_idx]
setup_data = npy.zeros([3,5,2,max_n_deg]) # [PCSetup|RAP|setup-RAP , node_idx, solv_type_idx, degree_idx]
node_arr = 5 * [0]
its_arr = npy.zeros([5,2,max_n_deg]) # [node_idx, solv_type_idx, degree_idx]
N_arr = npy.zeros([5,max_n_deg])
solve_data[:,:,:] = npy.nan
setup_data[:,:,:,:] = npy.nan
max_nnode_idx = 0
lang = ['HIP','Kokkos/HIP']
langtag = ['hip','kokkos']
degree_min=2
ndegree_sets = 1
for filename in sys.argv[1:]:
    base = filename.split('.')
    words = base[0].split('-')
    print(words)
    nnodes = int(words[1])
    machine = 'Frontier'
    solver = words[6]
    if solver == 'hypre': solv_type_idx = 0
    else: solv_type_idx = 1
    FEQ = words[2]
    ppn = words[4]
    nppn = int(ppn[0]);
    degree = int(FEQ[1])
    degreeidx = degree - degree_min
    if degreeidx + 1 > ndegree_sets : ndegree_sets = degreeidx + 1
    node_id = int(math.log(nnodes,8)) # index of num node (x) index
    if node_id > max_nnode_idx : max_nnode_idx = node_id 
    #print('nnodes,node_id = ',nnodes,node_id)
    node_arr[node_id] = nnodes
    #print ('numprocs=', nnodes, node_id, machine, solver, degree_id)
    for text in open(filename,"r"): 
        words = text.split()
        n = len(words)
        if n > 2 and words[0] == 'KSPSolve':
            stime = float(words[3])
            solve_data[node_id,solv_type_idx,degreeidx] = stime # clobbers to get last
        elif n > 1 and words[0] == 'PCSetUp':
            stime = float(words[3])
            setup_data[0,node_id,solv_type_idx,degreeidx] = stime # clobbers to get last
        elif n > 1 and words[0] == 'MatPtAPSymbolic':
            stime = float(words[3])
            setup_data[1,node_id,solv_type_idx,degreeidx] = stime
        elif n > 1 and words[0] == 'GAMG' and  words[1] == 'PtAP':
            stime = float(words[4])
            setup_data[2,node_id,solv_type_idx,degreeidx] = stime
        elif n > 7 and words[2] == 'converged':
            it = int(words[7])
            its_arr[node_id,solv_type_idx,degreeidx] = it
        elif n > 2 and words[0] == 'Number' and words[1] == 'equations':
            print (words)
            N_arr[node_id,degreeidx] = int(words[4])

    idx = idx + 1
if idx == 0: print ('no DATA -- need files as arguments !!!!!!')
solver_names_short = ['hypre','GAMG']
#print('max_nnode_idx=',max_nnode_idx)
#print('solve_data=',solve_data)
#print('setup_data=',setup_data)
#print('N=',N_arr)
its_arr_int = its_arr.astype(int)
print('its_arr=',its_arr_int)
setup_data[0,:,1,:] = setup_data[0,:,1,:] + setup_data[1,:,1,:] - setup_data[2,:,1,:] # GAMG setup: GAMG + PtAP - GAMG-setup-PtAP
plot_data = npy.zeros([3,max_nnode_idx+1,2,ndegree_sets])
plot_data[0,:,:,:] = setup_data[0,:max_nnode_idx+1,:,:ndegree_sets]
plot_data[1,:,:,:] = solve_data[:max_nnode_idx+1,:,:ndegree_sets]
plot_data[2,:,:,:] = its_arr_int[:max_nnode_idx+1,:,:ndegree_sets] # not used
print (plot_data)
nnodes_array = node_arr[:max_nnode_idx+1]
print(nnodes_array)
stage_names = ['Set up','Solve']
ylabels = [ 'AMG setup time (sec)', 'Solve Time (sec) (rtol=$\mathbf{10^{-12}}$)', '# Iterations', 'RAP time']
xlabels = ['# ' + machine + ' nodes (32 AMD EPYC cores/node, 8 MI250X GCDs)', '# ' + machine + ' MI250X GCD nodes', '# Frontier nodes', '# ' + machine + ' MI250X GCD nodes']
type_names = ['AMG setup, weak scaling', 'Weak scaling solve time', 'Iteration count', 'Galerkin coarse grid construction']
type_tag = ['setup','solve','its','rap']
for degreeidx in range(ndegree_sets):
    FEQ = degreeidx + degree_min
    Qstr = 'Q' + str(FEQ)
    prob = ', 3D Laplacian, 1M ' + Qstr + ' cells/node'
    num = int(N_arr[max_nnode_idx,degreeidx] / (nnodes_array[max_nnode_idx])/ 1024)
    eq_proc = f"{num:,}" + 'K'
    solver_names_long = ['hypre - HIP ('+ eq_proc +' eqs/node)','GAMG - Kokkos/HIP ('+ eq_proc +' eqs/node)']
    print(solver_names_long)
    for idx in range(4):
        #print(solve_time[:max_refId_maxNodeID+1,:max_ref_id+1,idx])
        if idx == 2 :
            df =  pd.DataFrame(data=its_arr_int[:max_nnode_idx+1,:,degreeidx], index=nnodes_array, columns=solver_names_long)
            df2 = pd.DataFrame(data=its_arr_int[:max_nnode_idx+1,:,degreeidx], index=nnodes_array, columns=solver_names_short)
        elif idx == 3 :
            df =  pd.DataFrame(data=setup_data[1,:max_nnode_idx+1,1,degreeidx], index=nnodes_array, columns=solver_names_long[1:2])
            df2 = pd.DataFrame(data=setup_data[1,:max_nnode_idx+1,1,degreeidx], index=nnodes_array, columns=solver_names_short[1:2])
        else :
            df =  pd.DataFrame(data=plot_data[idx,:,:,degreeidx], index=nnodes_array, columns=solver_names_long)
            df2 = pd.DataFrame(data=plot_data[idx,:,:,degreeidx], index=nnodes_array, columns=solver_names_short)
        df2.index.name = 'Nodes'
        df2.columns.name = 'Solver:'
        ax = df.plot(lw=2, colormap='jet', marker='s', markersize=10, logx=True,logy=False,  grid=True, legend=False, fontsize=16)
        title = type_names[idx] + prob
        ax.set_title(title,pad=20, fontdict={'fontsize':16})
        patches, labels = ax.get_legend_handles_labels()
        ax.legend(patches, labels, loc='best', fontsize=16)
        xmin, xmax, ymin, ymax = plt.axis()
        #ymax = max_thing[idx]
        xmin, xmax, ymin, ymax = plt.axis([xmin*.9, xmax*1.1, 0, 1.1*ymax])
        ax.set_xlabel(xlabels[idx], fontdict={'fontsize':16})
        ax.set_ylabel(ylabels[idx], fontdict={'fontsize':16})
        plt.savefig('weak_scaling_' + type_tag[idx] + '_' + machine + '_' + Qstr + '.png',bbox_inches='tight')
    #latex table
        print(df2.to_latex(longtable=False,escape=False,float_format="{:0.2f}".format, caption=type_names[idx] + prob + ',  ' + machine, label='tab:' + '_' + machine + '_' + Qstr + '_' + type_tag[idx]))
