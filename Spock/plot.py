#!/usr/bin/env python
import sys, os, math, glob
import matplotlib.pyplot as plt
import numpy as npy
import matplotlib.ticker as ticker
import random as rand
from numpy import array
import locale
import pandas as pd
locale.setlocale(locale.LC_ALL, '')
plt.rcParams["font.size"] = 11
#plt.rcParams["font.weight"] = "bold"
#plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams.update({'font.size': 11})
#plt.rcParams.update({'font.weight': "bold"})
idx = 0
nprocs = npy.empty(6, dtype=int)
solve_timeT = npy.empty([6,4,2])
max_proc_id = -1
max_ref_id = -1
for filename in sys.argv[1:]:
    base = filename.split('.')
    words = base[0].split('_')
    print(words)
    nps = int(words[1])
    proc_id = int(math.log(nps,8))
    if proc_id > max_proc_id: max_proc_id = proc_id
    run_type = words[3]
    refine_id = int(words[4]) - 2
    if refine_id > max_ref_id: max_ref_id = refine_id
    nprocs[proc_id] = nps
    if run_type == 'CPU': type_id = 0
    else : type_id = 1
    #print ('numprocs=', nps, run_type, run_type_str, run_sub_type,sub_type_str
    idx2 = 0
    for text in open(filename,"r"): 
        words = text.split()
        n = len(words)
        if n > 1 and words[0] == 'KSPSolve':
            idx2 = idx2 + 1
            if idx2 == 2:
                stime = float(words[3])
                solve_timeT[proc_id,refine_id,type_id] = stime
    idx = idx + 1
if max_ref_id == -1: print ('no DATA -- need files as arguments !!!!!!')
solve_time = solve_timeT[0:max_proc_id+1,0:max_ref_id+1,:]
marks2 = [['ro-','bo-.','mo--','go:'],['r^-', 'b^-.', 'm^--', 'g^:'],['r*-', 'b*-.', 'm*--', 'g*:'],['r+-', 'b+-.', 'm+--', 'g+:'],['rx-', 'bx-.', 'mx--', 'gx:'],['rv-', 'bv-.', 'mv--', 'gv:'],['rD-', 'bD-.', 'mD--', 'gD:'],['ro-', 'bo-.', 'mo--', 'go:'],['r|-', 'b|-.', 'm|--', 'g|:'],['rp-', 'bp-.', 'mp--', 'gp:'],['r*-', 'b*-.', 'm*--', 'g*:']]
#
# Weak scaling - GPU
#
series_name_full = ['2K cells/node', '16K cells/node', '128K cells/node', '1M cells/node']
series_name_short = ['2K', '16K', '128K', '1M']
nprocs = nprocs[0:max_proc_id+1];
type_name = ['PETSc (AMD EPYC)','Kokkos-HIP (AMD MI100)']
type_tag = ['cpu','gpu']
for idx in range(2):
    df = pd.DataFrame(data=solve_time[:,:,idx], index=nprocs, columns=series_name_full[0:max_ref_id+1])
    df2 = pd.DataFrame(data=solve_time[:,:,idx], index=nprocs, columns=series_name_short[0:max_ref_id+1])
    df2.index.name = 'Nodes'
    df2.columns.name = 'Cells/node:'
    #print df
    ax = df.plot(lw=2, colormap='jet', marker='s', markersize=10, logx=True,logy=True,  grid=True, legend=False, fontsize=16)
    title='3D Q2 Laplacian GAMG solve, ' + type_name[idx]
    ax.set_title(title,pad=20, fontdict={'fontsize':16})
    patches, labels = ax.get_legend_handles_labels()
    ax.legend(patches, labels, loc='upper left', fontsize=16)
    xmin, xmax, ymin, ymax = plt.axis()
    xmin, xmax, ymin, ymax = plt.axis([xmin*.9, xmax*1.1, 1.e-2, 200])
    ax.set_xlabel('# Spock nodes', fontdict={'fontsize':16})
    ax.set_ylabel('AMG Solve Time (rtol=$\mathbf{10^{-12}}$)', fontdict={'fontsize':16})
    plt.savefig('weak_scaling_amd_' + type_tag[idx] + '.png',bbox_inches='tight')
    #latex table
    print(df2.to_latex(longtable=False,escape=False,float_format="{:0.2f}".format, caption='Scaled speedup, 3D Q2 Laplacian GAMG solve time (10 solves) -- ' + type_name[idx], label='tab:amd_'+type_tag[idx]))

